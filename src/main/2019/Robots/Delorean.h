#pragma once
#include <iostream>
#include "Utilities/FluxVictor.h"
#include "Subsystems/FluxRobot.h"
#include "frc/XboxController.h"
#include "PID/PID.h"
#include <AHRS.h>
#include <frc/Timer.h>
#include "networktables/NetworkTableInstance.h"

struct TankMotorValues {
    double rightSpeed = 0;
    double leftSpeed = 0;
};

class TankKinematics {
public:
    static TankMotorValues calcInverseKinematics(double vX, double w){
        TankMotorValues values;
        values.rightSpeed = vX + w/2.0;
        values.leftSpeed = vX - w/2.0;
        return values;
    }
};

class Delorean : public FluxRobot {

    public:
        Delorean();

        void addProperties() override;
        void initSubsystems() override;

        void robotInit() override;
        void robotUpdate() override;

        void teleopInit() override;
        void teleopUpdate() override;

        void autonInit() override;
        void autonUpdate() override;

        void disabledInit() override;
        void disabledUpdate() override;

        void writeMotors(TankMotorValues values);
    private:
        double targetHeading = 0;

        FluxVictor motorHatchIntake = {1};

        FluxVictor motorLeft3 = {9};
        FluxVictor motorLeft1 = {2};
        FluxVictor motorLeft2 = {6};

        FluxVictor motorRight1 = {3};
        FluxVictor motorRight2 = {5};
        FluxVictor motorRight3 = {7};

        FluxVictor mech1 = {4};
        FluxVictor mech2 ={8};

        frc::XboxController* control = new frc::XboxController(0);
        AHRS* gyro = new AHRS(SPI::Port::kMXP);
        PID* headingController = new PID();
        PID* visionController = new PID();
        frc::XboxController* mechcontrol = new frc::XboxController(1);
        frc::DoubleSolenoid cargoPistons{4, 5};
        frc::DoubleSolenoid driveTrainPistons{6, 7};
        std::shared_ptr<NetworkTable> visionTable;

        frc::Compressor compressor;

        double lastTimeUpdate = frc::Timer::GetFPGATimestamp();
        double maxHeadingRate = 180.0; //Change per second
        double deltaT = 1.0/8.0;
        double yaw = 0.0;
        bool visionEnabled = false;
        
        double error = 0.0;
        double lastError = 0.0;
        double timeStep = 0.0;
};