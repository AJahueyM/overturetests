#include "2019/Robots/Delorean.h"
#include "frc/XboxController.h"

Delorean::Delorean() : FluxRobot("Delorean") {

}

void Delorean::initSubsystems() {

}

void Delorean::addProperties() {
    ;
}

void Delorean::robotInit() {
    visionTable = nt::NetworkTableInstance::GetDefault().GetTable("ChickenVision");
    compressor.Start();

    motorLeft1.ConfigOpenloopRamp(deltaT);
    motorLeft2.ConfigOpenloopRamp(deltaT);
    motorLeft3.ConfigOpenloopRamp(deltaT);
    motorRight2.ConfigOpenloopRamp(deltaT);
    motorRight1.ConfigOpenloopRamp(deltaT);
    motorRight3.ConfigOpenloopRamp(deltaT);

    motorRight2.SetInverted(true);
    motorRight1.SetInverted(true);
    motorRight3.SetInverted(true);
    motorLeft2.SetInverted(true);

    motorRight1.SetNeutralMode(NeutralMode::Brake);
    motorRight2.SetNeutralMode(NeutralMode::Brake);
    motorRight3.SetNeutralMode(NeutralMode::Brake);
    motorLeft1.SetNeutralMode(NeutralMode::Brake);
    motorLeft2.SetNeutralMode(NeutralMode::Brake);
    motorLeft3.SetNeutralMode(NeutralMode::Brake);
    mech2.SetInverted(true);

    headingController->setContinous(true);
    headingController->setUpperLimit(180.0);
    headingController->setLowerLimit(-180.0);

    headingController->setP(0.02);
    headingController->setI(0.0);
    headingController->setD(0.0075);

    visionController->setP(0.02);
    visionController->setI(0.0);
    visionController->setD(0.005);
    SmartDashboard::PutNumber("Heading P", headingController->getP());
    SmartDashboard::PutNumber("Heading I", headingController->getI());
    SmartDashboard::PutNumber("Heading D", headingController->getD());

    SmartDashboard::PutNumber("Vision P", visionController->getP());
    SmartDashboard::PutNumber("Vision I", visionController->getI());
    SmartDashboard::PutNumber("Vision D", visionController->getD());
}

void Delorean::robotUpdate() {
    yaw = -gyro->GetYaw();

    visionController->setP(SmartDashboard::GetNumber("Vision P", 0.02));
    visionController->setI(SmartDashboard::GetNumber("Vision I", 0.0));
    visionController->setD(SmartDashboard::GetNumber("Vision D", 0.005));

    headingController->setFeedback(yaw);

    if(targetHeading > 180.0){
        targetHeading -= 360;
    }

    if(targetHeading < -180.0){
        targetHeading += 360;
    }



    headingController->setTarget(targetHeading);

    double angular = 0.0;
    if(visionTable->GetBoolean("engaged", false) && visionEnabled){
        targetHeading = yaw - visionTable->GetNumber("YAW", 0.0);
        visionController->setFeedback(visionTable->GetNumber("YAW", 0.0));
        visionController->setTarget(0.0);
        angular = visionController->getOutput();
    }else{
        error = targetHeading - yaw;
        if(error > 180.0){
            error -= 360;
        }
        if(error < -180.0){
            error += 360;
        }
        
        double errorDelta = (error - lastError) / timeStep;
        angular =  error * headingController->getP() + errorDelta * headingController->getD();
        
        lastError = error;
    }

    double linear = -control->GetY(frc::GenericHID::JoystickHand::kLeftHand);

    TankMotorValues values = TankKinematics::calcInverseKinematics(linear, angular);
    writeMotors(values);

    SmartDashboard::PutNumber("Heading", yaw);
    SmartDashboard::PutNumber("Target Heading", targetHeading);
    SmartDashboard::PutNumber("Heading Error", headingController->getCurrentError());
    SmartDashboard::PutNumber("Heading Error Delta", headingController->getErrorDelta());
}

void Delorean::autonInit() {
    gyro->ZeroYaw();
    cargoPistons.Set(frc::DoubleSolenoid::Value::kReverse);
    targetHeading = 0;
    driveTrainPistons.Set(frc::DoubleSolenoid::Value::kForward);
}

void Delorean::autonUpdate() {
    teleopUpdate();
}

void Delorean::teleopInit() {

}

void Delorean::teleopUpdate() {
    double currentUpdate = frc::Timer::GetFPGATimestamp();
    timeStep = currentUpdate - lastTimeUpdate;
    SmartDashboard::PutNumber("Timestep", timeStep);
//    if(control->GetBumper(frc::GenericHID::JoystickHand::kLeftHand)){
//        driveTrainPistons.Set(frc::DoubleSolenoid::Value::kForward);
//    } 
//
//    if(control->GetBackButton()){
//        driveTrainPistons.Set(frc::DoubleSolenoid::Value::kReverse);
//    } 

    if(control->GetStartButton()){
        compressor.Stop();
    }


    double headingChange = -control->GetX(frc::GenericHID::JoystickHand::kRightHand) * maxHeadingRate * timeStep;
    visionEnabled = control->GetBumper(frc::GenericHID::JoystickHand::kRightHand);
    targetHeading += headingChange;

//    if(cargoPistons.Get() == frc::DoubleSolenoid::Value::kForward){
//      
//    }else{
//        mech1.Power(0.0);
//        mech2.Power(0.0);
//    }
//
    mech1.Power( -mechcontrol->GetY(frc::GenericHID::JoystickHand::kLeftHand));
    mech2.Power( -mechcontrol->GetY(frc::GenericHID::JoystickHand::kLeftHand));
//    if(mechcontrol->GetAButton()){
//        cargoPistons.Set(frc::DoubleSolenoid::Value::kForward);
//    }
//
//    if(mechcontrol->GetBButton()){
//        cargoPistons.Set(frc::DoubleSolenoid::Value::kReverse);
//    }
    motorHatchIntake.Power( -mechcontrol-> GetY(frc::GenericHID::JoystickHand::kRightHand));
    lastTimeUpdate = frc::Timer::GetFPGATimestamp();
}

void Delorean::disabledInit() {
    ;
}

void Delorean::disabledUpdate() {
    ;
}

void Delorean::writeMotors(TankMotorValues values) {
    motorLeft1.Power(values.leftSpeed*0.5);
    motorLeft2.Power(values.leftSpeed*0.5);
    motorLeft3.Power(values.leftSpeed*0.5);
    motorRight1.Power(values.rightSpeed*0.5);
    motorRight2.Power(values.rightSpeed*0.5);
    motorRight3.Power(values.rightSpeed*0.5);
}


