#include "Subsystems/FluxChassis.h"

FluxChassis::FluxChassis() : FluxSubsystem("FluxChassis") {
    r1.ConfigOpenloopRamp(ramp);
   r2.ConfigOpenloopRamp(ramp);
    l1.ConfigOpenloopRamp(ramp);
    l2.ConfigOpenloopRamp(ramp);
    l2.Set(ControlMode::Follower,l1.GetDeviceID());
    r2.Set(ControlMode::Follower,r1.GetDeviceID());
}

void FluxChassis::robotInit() {
   r1.SetInverted(false);
   r2.SetInverted(false);   
    l1.SetInverted(false);
   l2.SetInverted(false);
}

void FluxChassis::robotUpdate() {
}   

void FluxChassis::teleopInit() {
}

void FluxChassis::teleopUpdate() {

    double rightHand = xbox.GetY(XboxController::kRightHand);
    double leftHand = xbox.GetY(XboxController::kLeftHand);

     if(rightHand > 0.0 && rightHand < 0.09) {
        rightHand = 0.0;
    }
    if(rightHand < 0.0 && rightHand > -0.09) {
        rightHand = 0.0;
    }
    r1.Power(rightHand);
    l1.Power(leftHand);

    if (xbox.GetStartButtonPressed()) {
        compressor.Stop();
    }
}

void FluxChassis::autonInit() {
    ;
}

void FluxChassis::autonUpdate() {
    ;
}

void FluxChassis::disabledInit() {
    ;
}

void FluxChassis::disabledUpdate() {
    ;
}