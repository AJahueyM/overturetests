#pragma once
#include "PID/PID.h"
#include "Subsystems/FluxSubsystem.h"
#include "Utilities/FluxVictor.h"
#include "AHRS.h"
#include "frc/XboxController.h"
#include "frc/smartdashboard/SmartDashboard.h"
#include <chrono>

#include "Utilities/Gearbox.h"
#include "frc/Compressor.h"
using namespace std;


class FluxChassis : public FluxSubsystem {
    public:
        FluxChassis();
       
        void robotInit() override;
        void robotUpdate() override;

        void teleopInit() override;
        void teleopUpdate() override;

        void autonInit() override;
        void autonUpdate() override;
        
        void disabledInit() override;
        void disabledUpdate() override;
    private:
        frc::XboxController xbox{0};
        FluxVictor r1{1};
        FluxVictor r2{2};
        FluxVictor l1{3};
        FluxVictor l2{4};
        frc::Compressor compressor;
        int ramp = 1/4;
       
};
