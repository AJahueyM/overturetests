var hierarchy =
[
    [ "FluxSubsystem", "classFluxSubsystem.html", [
      [ "FluxRobot", "classFluxRobot.html", [
        [ "Delorean", "classDelorean.html", null ],
        [ "Pepito", "classPepito.html", null ]
      ] ],
      [ "PepoChassis", "classPepoChassis.html", null ]
    ] ],
    [ "SubsystemManager", "classSubsystemManager.html", null ],
    [ "TimedRobot", null, [
      [ "FluxRobot", "classFluxRobot.html", null ],
      [ "Robot", "classRobot.html", null ]
    ] ]
];